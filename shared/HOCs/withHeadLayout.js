import React, { Fragment, Component } from 'react';
import Head from 'next/head';
import ReactHtmlParser from 'react-html-parser';
import { APP_NAME } from '../../client/lib/client-config';

function generateTitle(title, subTitle) {
    if (subTitle) {
        return `${title} - ${subTitle}`;
    } else {
        return title;
    }
}

const headConfig = {
    subTitle: null,
    customMetas: []
};

function withHeadLayout(initialConfig = {}) {
    const config = {
        ...headConfig,
        ...initialConfig
    };

    return function(WrappedComponent) {
        return class extends Component {
            static getInitialProps(ctx) {
                if (WrappedComponent.getInitialProps) {
                    return WrappedComponent.getInitialProps(ctx);
                } else {
                    return {};
                }
            }

            render() {
                return (
                    <Fragment>
                        <Head>
                            <title key="title">
                                {generateTitle(APP_NAME, config.subTitle)}
                            </title>
                            {/* ADD HERE THE MANUAL METAS */}

                            {/* ADD HERE THE MANUAL METAS */}

                            {config.customMetas.map(metaTag =>
                                ReactHtmlParser(metaTag)
                            )}
                        </Head>
                        <WrappedComponent {...this.props} />
                    </Fragment>
                );
            }
        };
    };
}

export default withHeadLayout;
