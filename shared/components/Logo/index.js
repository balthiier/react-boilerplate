import React, { Fragment } from 'react';

function Logo(props) {
    return (
        <Fragment>
            <img
                className={'app-logo'}
                src={'/static/favicon.png'}
                {...props}
            />
            <style jsx>
                {`
                    .app-logo {
                        width: 150px;
                        height: 150px;
                        animation: rotation 2s infinite linear;
                    }

                    @keyframes rotation {
                        from {
                            transform: rotate(0deg);
                        }
                        to {
                            transform: rotate(360deg);
                        }
                    }
                `}
            </style>
        </Fragment>
    );
}

export default Logo;
