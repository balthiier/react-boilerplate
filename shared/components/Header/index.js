import React, { Component } from 'react';

class Header extends Component {
    render() {
        return <header {...this.props}>{this.props.children}</header>;
    }
}

export default Header;
