import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import authentication from './reducers/authentication';

export function initializeStore(initialState) {
    const isDev = process.env.NODE_ENV === 'development';

    return createStore(
        combineReducers({ authentication }),
        initialState,
        isDev
            ? composeWithDevTools({ name: 'REDUXZAUM' })(applyMiddleware(thunk))
            : applyMiddleware(thunk)
    );
}
