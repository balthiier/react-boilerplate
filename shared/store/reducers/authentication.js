const initialState = {
    token: null
};

export default (state = initialState, action) => {
    switch (action) {
        default:
            return state;
    }
};
