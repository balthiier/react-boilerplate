const initialState = {
    hello: 'world'
};

export default (state = initialState, action) => {
    switch (action) {
        default:
            return state;
    }
};
