import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import classes from './style.scss';
import Header from '../../components/Header';
import Logo from '../../components/Logo';

class HeaderContainer extends Component {
    constructor() {
        super();

        this.titleRef = React.createRef();
        this.descriptionRef = React.createRef();
    }

    componentDidMount() {
        this.renderLettersSequence(this.titleRef.current, 0);
        this.renderLettersSequence(this.descriptionRef.current, 150);
    }

    renderLettersSequence(ref, delay) {
        const letters = ref.innerText.split('');
        const lettersReactElements = [];
        ref.innerText = ''; // clean the raw text

        letters.forEach((letter, i) => {
            const letterSpanTag = React.createElement(
                'span',
                {
                    className: cx(classes.letter),
                    style: {
                        animationDelay: 150 * i + delay + 'ms'
                    },
                    key: i
                },
                letter
            );

            lettersReactElements.push(letterSpanTag);

            if (i >= letters.length - 1) {
                ReactDOM.render(
                    lettersReactElements,
                    ReactDOM.findDOMNode(ref)
                );
            }
        });
    }

    render() {
        return (
            <div>
                <Header className={cx(classes.header)}>
                    <Logo />
                    <h1 ref={this.titleRef}>REACT</h1>
                    <h2 ref={this.descriptionRef}>BOILERPLATE</h2>
                </Header>
            </div>
        );
    }
}

export default HeaderContainer;
