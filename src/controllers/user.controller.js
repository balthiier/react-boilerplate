const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { APP_SECRET } = require('../../src/config/app.config');

const generateUserToken = user => {
    return jwt.sign(
        { ...user },
        APP_SECRET
        // { expiresIn: '1h' } // TODO: add expire and refresh token logic
    );
};

const signIn = async (req, res) => {
    try {
        const { email, password: rawPassword } = req.body;
        const user = await User.findOne({ email });

        if (!user) {
            return res.status(404).json({
                status: res.statusCode,
                message: 'user does not exist'
            });
        }

        const isMatch = await user.comparePassword(rawPassword);

        if (!isMatch) {
            return res
                .status(400)
                .json({ status: res.statusCode, message: 'wrong credentials' });
        }

        const matchedUser = await User.findOne({ email }).select('-password');

        // test a matching password
        res.status(200).json({
            accessToken: generateUserToken(
                matchedUser.toObject({ getters: true })
            ),
            user: matchedUser
        });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
};

const signUp = async (req, res) => {
    try {
        const { username, email, password } = req.body;
        const user = async () =>
            await User.findOne({ email }).select('-password');

        if (await user()) {
            return res
                .status(400)
                .json({ status: res.statusCode, message: 'user exist' });
        }

        await User.create({ username, email, password });

        const newUser = await user();

        res.status(200).json({
            accessToken: generateUserToken(newUser.toObject({ getters: true })),
            user: newUser
        });
    } catch (error) {
        res.status(500).json(error);
    }
};

module.exports = {
    signIn,
    signUp
};
