const User = require('../models/User');

const showProfile = async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');

        res.status(200).json({
            user
        });
    } catch (error) {
        res.status(500).json(error);
    }
};

module.exports = {
    showProfile
};
