const express = require('express');
const router = express.Router();
const userProfileController = require('../controllers/profile.controller');

router.get('/user', userProfileController.showProfile);

module.exports = router;
