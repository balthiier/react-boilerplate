const express = require('express');
const router = express.Router();
const authRoutes = require('./auth');
const profileRoutes = require('./profile');
const booksRoutes = require('./books');

router.use('/auth', authRoutes);
router.use('/profile', profileRoutes);
router.use('/books', booksRoutes);

router.get('/status', (_, res) => {
    res.status(200).json({
        status: res.statusCode,
        message: "It's the job that's never started as takes longest to finish"
    });
});

router.all('*', (_, res) => {
    res.status(404).json({
        status: res.statusCode,
        message: 'Not found'
    });
});

module.exports = router;
