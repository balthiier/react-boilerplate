const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    return res
        .status(200)
        .json({ books: [{ title: '123' }, { title: '321' }] });
});

module.exports = router;
