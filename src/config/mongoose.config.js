const mongoose = require('mongoose');
const { MONGODB_URL } = require('../config/app.config');

mongoose.connect(
    MONGODB_URL,
    { useNewUrlParser: true, keepAlive: 120 },
    err => {
        if (err) {
            return console.error(err);
        }

        console.log('mongoose connected!');
    }
);

mongoose.set('useCreateIndex', true);
