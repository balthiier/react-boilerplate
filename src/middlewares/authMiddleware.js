const jwt = require('express-jwt');
const { APP_SECRET } = require('../../src/config/app.config');

const jwtMiddleware = jwt({
    secret: APP_SECRET
}).unless({ path: ['/api/status', '/api/auth/signin', '/api/auth/signup'] });

const jwtMiddlewareResult = (err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        return res.status(401).json({
            status: res.statusCode,
            message: 'Unauthorized request'
        });
    }

    next();
};

module.exports = {
    jwtMiddleware,
    jwtMiddlewareResult
};
