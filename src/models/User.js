const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { SALT_WORK_FACTOR } = require('../../src/config/app.config');
const { emailReg } = require('../../src/utils/regex');

const UserSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: true,
            minlength: 3
        },
        email: {
            type: String,
            required: true,
            unique: true,
            validate: {
                validator: function(value) {
                    return emailReg.test(value);
                }
            }
        },
        password: {
            type: String,
            required: true
        }
    },
    { versionKey: false }
);

UserSchema.pre('save', function(next) {
    let user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.method('comparePassword', async function(candidatePassword) {
    return await bcrypt.compareSync(candidatePassword, this.password);
});

module.exports = mongoose.model('User', UserSchema);
