import React, { Component } from 'react';
// import './style.scss'; // this imports the scss, another flavor

import HeaderContainer from '../../shared/containers/HeaderContainer';
import withHeadLayout from '../../shared/HOCs/withHeadLayout';

// This will configure the head for home page
const headConfig = {
    subTitle: 'Subtitle Page',
    customMetas: [
        '<meta name="metaname" content="initial-scale=1.0, width=device-width"/>'
    ]
};

class Home extends Component {
    static async getInitialProps({ req }) {
        return {};
    }

    render() {
        return <HeaderContainer />;
    }
}

export default withHeadLayout(headConfig)(Home);
