import React, { Component } from 'react';
// import styled from 'styled-components'; // this returns a component with div styled, another flavor
// import './style.scss'; // this imports the scss, another flavor

import HeaderContainer from '../shared/containers/HeaderContainer';
import withHeadLayout from '../shared/HOCs/withHeadLayout';

// This will configure the head for home page
const headConfig = {
    subTitle: 'Subtitle Page',
    customMetas: [
        '<meta name="metaname" content="initial-scale=1.0, width=device-width"/>'
    ]
};

class MainPage extends Component {
    render() {
        return <HeaderContainer />;
    }
}

export default withHeadLayout(headConfig)(MainPage);
