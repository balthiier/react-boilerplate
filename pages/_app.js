import React from 'react';
import { Provider } from 'react-redux';
import App, { Container } from 'next/app';
import Head from 'next/head';
import '../shared/scss/_guidelines.scss';

import { APP_NAME } from '../client/lib/client-config';
import withReduxStore from '../client/lib/withReduxStore';

class MyApp extends App {
    render() {
        const { Component, pageProps, router, reduxStore } = this.props;

        return (
            <Container>
                <Provider store={reduxStore}>
                    <Head>
                        <title key="title">{APP_NAME}</title>
                        <meta
                            name="viewport"
                            content="initial-scale=1.0, width=device-width"
                            key="viewport"
                        />
                        <link
                            rel="icon"
                            href="/static/favicon.png"
                            type="image/png"
                            key="favicon"
                        />
                    </Head>
                    <Component {...pageProps} router={router} />
                </Provider>
            </Container>
        );
    }
}

export default withReduxStore(MyApp);
