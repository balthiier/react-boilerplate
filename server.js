const express = require('express');
const { parse } = require('url');
const next = require('next');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const api = require('./src/routes');
const {
    jwtMiddleware,
    jwtMiddlewareResult
} = require('./src/middlewares/authMiddleware');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const server = express();
const handle = app.getRequestHandler(app);

app.prepare().then(() => {
    // request token from header to return calls
    server.use('/api', jwtMiddleware, jwtMiddlewareResult);
    // parse cookies
    server.use(cookieParser());
    // parse application/x-www-form-urlencoded
    server.use(bodyParser.urlencoded({ extended: true, limit: '1mb' }));
    // parse application/json
    server.use(bodyParser.json());
    server.use('/api', api);

    server.get('/home/:slug', (req, res) => {
        return app.render(req, res, '/home', { slug: req.params.slug });
    });

    server.all('*', (req, res) => {
        const parsedUrl = parse(req.url, true);

        handle(req, res, parsedUrl);
    });

    require('./src/config/mongoose.config');

    server.listen(3000, () => console.log(`> Ready on http://localhost:3000`));
});
